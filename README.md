# map_lib_ios

[![CI Status](http://img.shields.io/travis/Юрий Воскресенский/map_lib_ios.svg?style=flat)](https://travis-ci.org/Юрий Воскресенский/map_lib_ios)
[![Version](https://img.shields.io/cocoapods/v/map_lib_ios.svg?style=flat)](http://cocoapods.org/pods/map_lib_ios)
[![License](https://img.shields.io/cocoapods/l/map_lib_ios.svg?style=flat)](http://cocoapods.org/pods/map_lib_ios)
[![Platform](https://img.shields.io/cocoapods/p/map_lib_ios.svg?style=flat)](http://cocoapods.org/pods/map_lib_ios)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

map_lib_ios is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "map_lib_ios"
```

## Author

Юрий Воскресенский, i.voskresenskii@infotech.team

## License

map_lib_ios is available under the MIT license. See the LICENSE file for more info.
