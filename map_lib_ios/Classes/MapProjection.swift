//
//  MapProjection.swift
//  map_online_lib
//
//  Created by Alexey Ivankov on 08.09.16.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics

public protocol MapProjection
{
    func pointForCoordinate(_ coordinate: CLLocationCoordinate2D) -> CGPoint
    func coordinateForPoint(_ point: CGPoint) -> CLLocationCoordinate2D
    func pointsForMeters(_ meters: CLLocationDistance, atCoordinate coordinate: CLLocationCoordinate2D) -> CGFloat
    
  
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool
    
   
    func visibleRegion() -> MapVisibleRegion;
}
