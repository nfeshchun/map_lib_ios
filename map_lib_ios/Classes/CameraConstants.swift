//
//  CameraConstants.swift
//  map_online_lib
//
//  Created by Alexey Ivankov on 07.09.16.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation

public enum CameraConstants: Double
{
    case startLatitude = 55.753744 // Moscow Red Square
    case startLongtitude = 37.621253
    case zoom = 13.5 // zoom lvl for worst accuracy detection
    case bearing = 30.0
    case viewingAngle = 45.0
}
