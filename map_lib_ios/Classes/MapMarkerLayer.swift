//
//  MapMarkerLayer.swift
//  map_online_lib
//
//  Created by Steve Jobs on 22/10/2016.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation
import CoreLocation


public protocol MapMarkerLayer: class
{
    var latitude: CLLocationDegrees { get set }
    var longitude: CLLocationDegrees { get set }
}