//
//  IMapView.swift
//  map_online_lib
//
//  Created by Alexey Ivankov on 03.07.16.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

public protocol MapViewDelegate: class
{
    func mapView(_ mapView: MapView!, idleAtCameraPosition position: MapCameraPosition!);
    func mapView(_ mapView: MapView!, didTapMarker marker: MapMarker!) -> Bool;
    func mapView(_ mapView: MapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D)
    func mapView(_ mapView: MapView!, didChangeCameraPosition position: CLLocationCoordinate2D)
}

public protocol MapView : class
{
    var view: UIView { get }
    
    weak var delegate:MapViewDelegate? { get set }
    var projection: MapProjection { get }
    var cameraPosition:MapCameraPosition { get }
    
    func myLocation() -> CLLocation!;
    func setMyLocationEnabled(_ enabled: Bool);
    func clean();
    
    func focusMyLocation()
    func addMarker(_ marker:MapMarker)
    func addOverlay(_ overlay: MapOverlay)
    func addTileLayer(_ tileLayer: MapTileLayer)
    func removeMarker(_ marker:MapMarker)
    
    func buildPath(_ mapPath: MapPath, strokeColor: UIColor, strokeWidth: CGFloat)
    
    func handleMyLocation(_ completion: @escaping (_ locationCoordinates: CLLocationCoordinate2D)->(Void));
    func stopHandleMyLocation();
    
    func zoomIn();
    func zoomOut();
    
    func animateToCurrentLocation();
    func animateToLocation(_ location: CLLocationCoordinate2D, zoom: Float)
    func animateToZoom(_ zoom: Float);
    
    func updateCameraPadding(_ padding: UIEdgeInsets)
}
