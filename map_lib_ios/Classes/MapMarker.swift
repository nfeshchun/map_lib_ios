//
//  MapMarker.swift
//  map_online_lib
//
//  Created by Alexey Ivankov on 07.09.16.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics
import UIKit

public protocol MapMarker : class
{
    var position: CLLocationCoordinate2D { get set }
    var snippet: String? { get set }
    var icon: UIImage? { get set }
    var iconView: UIView { get set }
    var tracksViewChanges: Bool { get set }
    var tracksInfoWindowChanges: Bool { get set }
    var groundAnchor: CGPoint { get set }
    var infoWindowAnchor: CGPoint { get set }
    var draggable: Bool { get set }
    var flat: Bool { get set }
    var rotation: CLLocationDegrees { get set }
    var opacity: Float { get set }
    var userData: Any? { get set }
    var markerLayer: MapMarkerLayer { get }
    func removeFromMap()
    
    var zIndex: Int32 {get set}
    
    func markerImageWithColor(_ color: UIColor?) -> UIImage;
}

