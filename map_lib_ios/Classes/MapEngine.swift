//
//  IMapEngine.swift
//  map_online_lib
//
//  Created by Alexey Ivankov on 03.07.16.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation

public protocol MapEngine : class
{
    func startConfiguration(apiKey: String)
}
