//
//  MapPath.swift
//  Pods
//
//  Created by Steve Jobs on 26/09/16.
//
//


import Foundation
import CoreLocation
import CoreGraphics
import UIKit

public protocol MapPath : class
{
    func count() -> UInt
    func coordinateAtIndex(_ index: UInt) -> CLLocationCoordinate2D
    func addCoordinate(_ coord: CLLocationCoordinate2D)
}

