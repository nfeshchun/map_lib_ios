//
//  MapCameraPosition.swift
//  map_online_lib
//
//  Created by Alexey Ivankov on 07.09.16.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics

public protocol MapCameraPosition
{    
     var target: CLLocationCoordinate2D { get }
     var zoom: Float { get }
     var bearing: CLLocationDirection { get }
     var viewingAngle: Double { get }
    
    static func zoomAtCoordinate(_ coordinate: CLLocationCoordinate2D, forMeters meters: CLLocationDistance, perPoints points: CGFloat) -> Float;
    
}

