//
//  MapCoordinateBounds.swift
//  map_online_lib
//
//  Created by Steve Jobs on 21/10/2016.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

import Foundation
import CoreLocation


public protocol MapCoordinatesBounds
{
    var nearLeft: CLLocationCoordinate2D { get }
    var farRight: CLLocationCoordinate2D { get }
    
    func containsCoordinate(_ coordinate: CLLocationCoordinate2D) -> Bool
}
