//
//  MapOverlay.swift
//  Pods
//
//  Created by Steve Jobs on 15/02/2017.
//
//

import Foundation


public protocol MapOverlay : class
{
    func removeFromMap()
}

