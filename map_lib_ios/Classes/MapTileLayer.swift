//
//  MapTileLayer.swift
//  map_online_lib
//
//  Created by Steve Jobs on 18/05/2017.
//  Copyright © 2017 Alexey Ivankov. All rights reserved.
//

import Foundation
import UIKit


public protocol MapTileLayer
{
    func removeFromMap()
    weak var delegate: MapTileLayerDelegate? { get set }
    func clearTileCache()
    var sizeOfTile: CGFloat { get set } // size of tile in pt
}


public protocol MapTileLayerDelegate: class
{
    func requestTileFor(cell: TileCell, showImage: @escaping (UIImage?)->Void)
    func clearTileCache()
}

